package com.example.android.architecture.blueprints.todoapp.statistics

import com.example.android.architecture.blueprints.todoapp.data.Task
import org.junit.Assert.assertEquals
import org.junit.Test

class StatisticsUtilsTest {

    @Test
    fun getTaskStatus() {

        val tasks = listOf<Task>(
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                )
        )

        val result = getActiveAndCompletedStats(tasks)

        assertEquals(result.completedTasksPercent, 0f)

    }

    @Test
    fun getActiveAndCompletedStats() {
        val tasks = listOf<Task>(
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                ),
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                ),
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                ),
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                ),
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                )
        )

        val result = getActiveAndCompletedStats(tasks)

        assertEquals(0f, result.completedTasksPercent)
    }

    @Test
    fun getEmptyTaskStatus() {

        val tasks = listOf<Task>(
                Task(
                        title = "Hello",
                        description = "How are you",
                        isCompleted = false
                )
        )

        val result = getActiveAndCompletedStats(tasks)

        assertEquals(result.completedTasksPercent, 0f)

    }

}